/**
 * Circle Painter
 * by Matt Katz.
 * Click on the image to put origin points on the face.
 * I'd recommend you put them on eyes and lips, as you want detail there.
 * press o to begin rendering out the image..
 * press p to render a
 * We'll paint out the image using scatterings of concentric circles,
 * larger ones where don't need detail.
 * inspired by this image: http://www.flickr.com/photos/tsevis/2498089530/sizes/l/
 *
 */

 /**
  * TODO IDEA LIST
  * Negative spaces - right click to set areas to NOT render in
  * Drag and drop a picture into the space
  */

let source;
// These are points of interest, where we want high detail
let points = [];
// These are points where we want to prevent rendering. Negative render points!
let negPoints = [];
// are we currently rendering live to the screen with full control?
let liveRender = false;
// are we in a standard render?
let isInStandardRender = false;
//what is the smallest circle we should draw
let minRadius;
//the offscreen buffer/canvas we can draw on
let buffer;
//the scale factor from source to display
let sourceScaleFactor;
//how much do we want to scale up the image?
let outputScaleFactor;
//scale factor from buffer to display
let bufferScaleFactor;
//this will constrain new circles to a maximum distance from an origin point;
//How thick should the band of each circle be?
let maxDistanceFromPoints;
//How thick should the band of each circle be?
let radiusStepSize;
// how many circles have we made?
let circleCount;
// Text to render down from the top;
let topText = [];
// Text to render up from the bottom;
let bottomText = [];
function preload() {
  source = loadImage("assets/boat_people.jpeg");
}
function setup() {
  cnv = createCanvas(windowWidth, windowHeight);
  cnv.mousePressed(canvasClick);
  outputScaleFactor = 2.0;
  buffer = createGraphics(outputScaleFactor * source.width, outputScaleFactor * source.height);
  buffer.noStroke();
  buffer.smooth();
  //TODO: pre-load a saved buffer?
  noStroke();
  smooth();

  //let's figure out the buffer and source scalefactors
  let wscalefactor = float(width)/float(source.width);
  let hscalefactor = float(height)/float(source.height);
  sourceScaleFactor = min(hscalefactor,wscalefactor);
  console.log("Scale factor: " + sourceScaleFactor);
  //this is the scale factor from the buffer to the display
  bufferScaleFactor = sourceScaleFactor/outputScaleFactor;
  //points = new pointsArray();
  minRadius = 4;
  maxDistanceFromPoints = max(buffer.width,buffer.height);
  radiusStepSize = 32;
  circleCount = 0;
  renderBaseScreen();
}
// When we click on the canvas, store an origin point
function canvasClick(){
  let newOne = createVector(mouseX/bufferScaleFactor,mouseY/bufferScaleFactor);
  switch(mouseButton){
    case LEFT:
      points.push(newOne);
      console.log("adding a new vector: " + newOne);
      break;
    case RIGHT:
      //TODO: add a negative point
      console.log("add a negative point");
      negPoints.push(newOne);
      return false;
      break;
    case CENTER:
      break;

  }
}

function keyPressed(){
  switch(key){
    case "s": // save the buffer
        console.log("saving the buffer");
        buffer.save(bufferName());
        break;
    case "o": // begin rendering;
        liveRender = ! liveRender;
        isInStandardRender = false;
    break;
    case "p": // play a good standard way to do render
        liveRender = false;
        isInStandardRender = true;
        background(255);
        buffer.background(255);
        topText = [];
        topText.push("Rendering");
        circleCount=0;
    break;
    case "c": // clear the origin points;
        points = [];
    break;
  }
  switch(keyCode){
    case UP_ARROW: // increase max dist from render point
        maxDistanceFromPoints=min(max(buffer.width,buffer.height),2 * maxDistanceFromPoints);
    break;
    case DOWN_ARROW: // Decrease max dist from render point
        maxDistanceFromPoints = max(int(maxDistanceFromPoints/2),minRadius*8);
    break;
    case RIGHT_ARROW: // widen the circle bands
        radiusStepSize = radiusStepSize*2;
    break;
    case LEFT_ARROW: // tighten the circle bands, but not less than 1
        radiusStepSize = max(1,int(radiusStepSize/2))
    break;
  }
}

function draw() {
  if (liveRender){
    background(255);
    image(renderPoints(source, buffer, 60), 0, 0, bufferScaleFactor*buffer.width, bufferScaleFactor*buffer.height);
    bottomText = statsText();
    renderScreenText(topText,bottomText);
  }
  else if ( isInStandardRender){
    background(0);
    standardRender(circleCount);
    bottomText = statsText()
    renderScreenText(topText,bottomText);
  }
  else{
    renderBaseScreen();
  }
  //background(50);

}

function standardRender(currentCount){
  //first we check if there are any origin points.
  //this won't work if there aren't any!
  let centers = points.length;

  if(centers == 0){
    return;
  }
  radiusStepSize = 2;

  let step = int(sqrt(maxDistanceFromPoints)) - 3;

  // I really don't know what I'm measuring here, naming is hard
  let densityMeasure = floor(currentCount/(3000  * centers));

  //really we are looking for something that scales number of circles as the area grows. Which has a relationship to the square of the radius.

  maxDistanceFromPoints = 32 * (pow(2,densityMeasure));
  image(renderPoints(source, buffer, 60), 0, 0, bufferScaleFactor*buffer.width, bufferScaleFactor*buffer.height);
  if (maxDistanceFromPoints > (max(buffer.width, buffer.height)/bufferScaleFactor)){
    isInStandardRender = false;
    buffer.save(bufferName());
  }

}

function shadowText(input, x, y){
  push();
  fill(0);
  text(input, x,y);
  fill(255);
  text(input, x+2, y+2);
  pop();
}

function renderBaseScreen(){
  background(0);
  image(source,0,0,source.width*sourceScaleFactor,source.height*sourceScaleFactor);
  renderOriginPoints();
  topText = helpText();
  bottomText = statsText();
  renderScreenText(topText, bottomText);
}

function renderScreenText(top, bottom){
  let tsize = int(height/30);
  textSize(tsize);
  let rowsFromTop = 1;
  for(const text of top){
    shadowText(text, tsize, tsize*rowsFromTop);
    rowsFromTop +=1;
  }

  let rowsFromBottom = 1;
  for(const text of bottom){
    shadowText(text, tsize, height - tsize*rowsFromBottom);
    rowsFromBottom +=1;
  }
}

function helpText(){
  value = [];
  value.push("Start/Stop rendering by pressing 'o'");
  value.push("Click areas where you want high detail");
  value.push("Press 's' to save a frame in the sketch folder.");
  value.push("Try rendering first, waiting, then 'o', add points, then 'o'");
  value.push("Up and Down constrain circle generation nearer and farther from detail clicks");
  return value;
}

function statsText(){
  value = [];
  value.push("Max Dist from points: " + maxDistanceFromPoints);
  value.push("Radius Step Size: "+ radiusStepSize);
  value.push("Rendering: "+liveRender + " rendered "+ circleCount + " circles");
  return value;
}
function renderStats(){
  let tsize = int(height/30);
  textSize(tsize);
  shadowText("Max Dist from points: " + maxDistanceFromPoints, tsize, height-tsize);
  shadowText("Radius Step Size: "+ radiusStepSize, tsize, height - 2*tsize);
  shadowText("Rendering: "+liveRender + " rendered "+ circleCount + " circles", tsize, height - 3*tsize);
}

function renderHelpText(){
  let tsize = int(height/20);
  textSize(tsize);
  shadowText("Start/Stop rendering by pressing 'o'", tsize,tsize);
  shadowText("Click areas where you want high detail", tsize, 2*tsize);
  shadowText("Press 's' to save a frame in the sketch folder.", tsize, 3*tsize);
  shadowText("Try rendering first, waiting, then 'o', add points, then 'o'", tsize, 4*tsize);
  shadowText("Up and Down constrain circle generation nearer and farther from detail clicks", tsize, 5*tsize);

}

function renderOriginPoints(){
  //world's coolest color
  push();
  fill("#bada55");
  for(  i = 0; i < points.length; i++){
    current = points[i];
    ellipse(current.x*bufferScaleFactor, current.y*bufferScaleFactor, minRadius,minRadius);
  }
  fill("#f00");
  for(  i = 0; i < negPoints.length; i++){
    current = negPoints[i];
    ellipse(current.x*bufferScaleFactor, current.y*bufferScaleFactor, minRadius,minRadius);
  }
  pop();
}

// dynamically generate a buffer name based on the date/time
function bufferName(){
  return "seriousbusiness"+nf(year())+ "-"+ nf(month(),2) +"-"+ nf(day(),2)+"-"+nf(hour(),2)+nf(minute(),2)+nf(second(),2)+".jpg";
}

//call pointilize using the inputSource on the currentState a number of renders, then return
//handle the beginDraw endDraw
function renderPoints(inputSource, currentState, numberOfRenders){
  for (let i = 0; i< numberOfRenders; i++){
    pointilizeImage(inputSource, currentState);
    //console.log("rendering point " + i + " of " + numberOfRenders);
    circleCount++;
  }
  return currentState;
}

// get a random point to render on the canvas.
// if max_distance is >0 make sure any point generated is less than that from
// a render point.
function getPointToRender(cnvs, pnts, maxDistance){
  let candidate = generateRandomPoint(cnvs.width, cnvs.height);
  candidateCount=1;

  //if the maxDistance is zero, we just return a random point.
  //if there are no origin points, we just return a random point
  while( ! isPointOK(candidate, pnts, negPoints, maxDistance)){
    candidate = generateRandomPoint(cnvs.width, cnvs.height);
  }
  //console.log("candidateCount: " + candidateCount + " maxDistance: " + maxDistance);
  return candidate;
}

// check if a point is less than the max distance from any detail point
// check if a point is closer than halfway between nearest detail point and negative point
function isPointOK(candidate,points, nPoints, maxDistance){
  // What do we do if there are no detail  points?
  //what do we do if there no negPoints?
  let distanceToNearestRenderPoint = int(minDist(candidate));
  if(distanceToNearestRenderPoint > maxDistance){
    return false;
  }
  // Check to see if the point is half way between this and the nearest negPoint.
  if(int(minDistBetween(candidate, nPoints)) < distanceToNearestRenderPoint ){
    return false;
  }

  return true;

}

function generateRandomPoint(x, y){
    return createVector(int(random(x)), int(random(y)));
}

function pointilizeImage(inputSource, currentState){
  //generate a random point that we are going to pointillize
  generatedPoint = getPointToRender(currentState, points, maxDistanceFromPoints);

  //what is the smallest distance we are from an origin point
  distance = minDist(generatedPoint);

  //let's generate a size based on that distance
  //we want that size to tend to be proportional to the distance
  //but allow for some random variation in size
  circleSize = max(minRadius,(distance/8)) * produceNormalDistributionNumber();
  while(circleSize >= minRadius){
    drawGraphicsCircle(inputSource, currentState, generatedPoint.x, generatedPoint.y, circleSize);
    circleSize = circleSize - getRadiusStep();
  }
}

function getRadiusStep(){
  return radiusStepSize;
}

//draw a circle the color of a point along the circumference.
function drawGraphicsCircle(inputSource, canvas, x, y, radius){
  radius = max(ceil(radius), minRadius);
  //scale from buffer x,y to inputsource x,y
  sourceColor = getColorFromSourceCircle(inputSource, int(x/outputScaleFactor), int(y/outputScaleFactor),int(radius/outputScaleFactor));
  canvas.fill(sourceColor);
  canvas.ellipse(x,y,radius,radius);
}

function getColorFromSourceCircle(inputSource, x, y, radius){
  //we want to get a point that is radius away from x,y
  //lets pic a point along the radius that we are intersted in
  //one before the next circle will get drawn.
  radius = random(floor(radius-1), radius) + radius;

  //let's pic a random angle around the circle.
  angle = floor(random(0,360));

  //do a little trig
  newX = floor((cos(angle) * radius)) + x;
  newY = floor((sin(angle) * radius)) + y;
  //int c = inputSource.pixels[y+x*inputSource.width];
  //return c;
  return inputSource.get(newX, newY);
}

//find the minimum distance from an origin point to this point p
function minDist(p){
  smallestDistance = max(buffer.width,buffer.height);
  smallestDistance = minDistBetween(p, points);
  return smallestDistance;
}

function minDistBetween(p, pointsArray){
  smallestDistance = max(buffer.width,buffer.height);
  for(i = 0; i < pointsArray.length; i++){
    current = pointsArray[i];
    smallestDistance = min(smallestDistance, dist(p.x, p.y, current.x, current.y));
  }
  return smallestDistance;
}

function produceNormalDistributionNumber(){
  a = random(0,2);
  b = random(0,2);
  c = random(0,2);
  d = random(0,2);
  return (a + b + c + d)/4;
}
